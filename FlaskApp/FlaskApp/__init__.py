from flask import Flask, render_template, request, url_for, session, redirect
from werkzeug.utils import secure_filename
from flask_mysqldb import MySQL
import sys
import os
#import yaml
app = Flask(__name__)

UPLOAD_FOLDER = 'static/photos/'
ALLOWED_EXTENSIONS = {'jpg', 'pdf', 'jpeg', 'png'}

# Configure db
#db = yaml.load(open('db.yaml'))
app.config['MYSQL_HOST'] = '130.240.200.41' #'localhost' #db['mysql_host']
app.config['MYSQL_USER'] = 'kalabalik' #db['mysql_user']
app.config['MYSQL_PASSWORD'] = 'kalabalik' #db['mysql_password']
app.config['MYSQL_DB'] = 'DABAS' #db['mysql_db']
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

mysql = MySQL(app)

@app.route('/user', methods=['GET', 'POST'])
def getUser():
    if not 'loggedin' in session:
        return redirect(url_for('start_page'))
    message="Ändra profilinställningar"
    if request.method == "POST":
        cur = mysql.connection.cursor()
        if request.form["submitButton"] == "Ändra inställningar":
            userDetails = request.form
            emailInput = userDetails["email"]
            passwordInput = userDetails["password"]
            nameInput = userDetails["name"]
            phoneNumberInput = userDetails["number"]
            try:
                cur.execute("UPDATE Users SET"
                            " email= CASE WHEN '{0}' != '' THEN '{0}' ELSE email END,"
                            " password= CASE WHEN '{1}' != '' THEN '{1}' ELSE password END,"
                            " name= CASE WHEN '{2}' != '' THEN '{2}' ELSE name END,"
                            " phoneNumber= CASE WHEN '{3}' != '' THEN '{3}' ELSE phoneNumber END"
                            " WHERE userID = {4}".format(emailInput, passwordInput, nameInput, phoneNumberInput, session['id']))
                mysql.connection.commit()
                message = "Ändringar sparade"
            except:
                message = "Något gick fel"
            cur.close()

        if request.form["submitButton"] == "Logga ut":
            session.pop('loggedin', None)
            session.pop('id', None)
            session.pop('name', None)
            return redirect(url_for('start_page'))
        if request.form["submitButton"] == "Lägg till admin":
            user = request.form['assignAdmin']
            cur.execute("UPDATE Users SET role='Admin' WHERE userID={0}".format(user))
            mysql.connection.commit()
            cur.close()
        if request.form["submitButton"] == "Ta bort admin":
            user = request.form['removeAdmin']
            cur.execute("UPDATE Users SET role='Customer' WHERE userID={0}".format(user))
            mysql.connection.commit()
            cur.close()
        if request.form["submitButton"] == "Ta bort användare":
            user = request.form['deleteUser']
            cur.execute("DELETE FROM Users WHERE userID={0}".format(user))
            mysql.connection.commit()
            cur.close()
        if request.form["submitButton"] == "Hämta ordrar":
            user = request.form['getOrders']
            cur = mysql.connection.cursor()

            cur.execute('SELECT role from Users WHERE userID={0}'.format(session['id']))
            clientRole = cur.fetchall()

            resultValue = cur.execute('SELECT DISTINCT orderID from Orders WHERE userID={0}'.format(user))

            if resultValue > 0:
                orderIDDetails = cur.fetchall()
                productNameArray = []
                orderArray = []
                orderLen = []

                for orderIDs in orderIDDetails:
                    cur.execute('SELECT productName FROM Products WHERE productID in (SELECT productID FROM Orders WHERE orderID={0})'.format(orderIDs[0]))
                    productDetails = cur.fetchall()
                    productNameArray.append(productDetails)

                    resultValue = cur.execute('SELECT * from Orders WHERE orderID={0}'.format(orderIDs[0]))
                    orderDetails  = cur.fetchall()
                    orderLen.append(len(orderDetails))
                    orderArray.append(orderDetails)
                cur.execute('SELECT userID, email from Users WHERE role="Customer"')
                customerList = cur.fetchall()
                cur.execute('SELECT userID, email from Users WHERE role="Admin"')
                adminList = cur.fetchall()
                cur.close()
                return render_template("user.html", message=message, orderDetails=orderDetails, productNameArray=productNameArray, orderArray=orderArray, orderLen=len(productNameArray), productLen=orderLen, clientRole=clientRole, customerList=customerList, adminList=adminList)
            cur.execute('SELECT userID, email from Users WHERE role="Customer"')
            customerList = cur.fetchall()
            cur.execute('SELECT userID, email from Users WHERE role="Admin"')
            adminList = cur.fetchall()
            cur.close()
            return render_template("user.html", message=message, noOrders="Inga ordrar", orderLen=0, productLen=0, clientRole=clientRole, customerList=customerList, adminList=adminList)

    cur = mysql.connection.cursor()

    cur.execute('SELECT role from Users WHERE userID={0}'.format(session['id']))
    clientRole = cur.fetchall()

    resultValue = cur.execute('SELECT DISTINCT orderID from Orders WHERE userID={0}'.format(session['id']))

    if resultValue > 0:
        orderIDDetails = cur.fetchall()
        productNameArray = []
        orderArray = []
        orderLen = []

        for orderIDs in orderIDDetails:
            cur.execute('SELECT productName FROM Products WHERE productID in (SELECT productID FROM Orders WHERE orderID={0})'.format(orderIDs[0]))
            productDetails = cur.fetchall()
            productNameArray.append(productDetails)

            resultValue = cur.execute('SELECT * from Orders WHERE orderID={0}'.format(orderIDs[0]))
            orderDetails  = cur.fetchall()
            orderLen.append(len(orderDetails))
            orderArray.append(orderDetails)
        cur.close()
        if clientRole[0][0] == 'Admin':
            cur = mysql.connection.cursor()
            cur.execute('SELECT userID, email from Users WHERE role="Customer"')
            customerList = cur.fetchall()
            cur.execute('SELECT userID, email from Users WHERE role="Admin"')
            adminList = cur.fetchall()
            return render_template("user.html", message=message, orderLen=0, productLen=0, clientRole=clientRole,customerList=customerList, adminList=adminList)
        return render_template("user.html", message=message, orderDetails=orderDetails, productNameArray=productNameArray, orderArray=orderArray, orderLen=len(productNameArray), productLen=orderLen, clientRole=clientRole)
    cur.close()
    if clientRole == 'Admin':
        cur = mysql.connection.cursor()
        cur.execute('SELECT userID, email from Users WHERE role="Customer"')
        customerList = cur.fetchall()
        cur.execute('SELECT userID, email from Users WHERE role="Admin"')
        adminList = cur.fetchall()
        cur.close()
        return render_template("user.html", message=message, noOrders="Inga ordrar", orderLen=0, productLen=0, clientRole=clientRole, customerList=customerList, adminList=adminList)
    return render_template("user.html", message=message, noOrders="Inga ordrar", orderLen=0, productLen=0, clientRole=clientRole)

@app.route('/product/<ID>', methods=['GET', 'POST'])
def getProd(ID):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM Products WHERE productID="{0}"'.format(str(ID)))
    prodData = cur.fetchall()
    #cur.execute('SELECT Reviews.userID, Reviews.productID, Reviews.grade, Reviews.content, Users.name'
    #            ' FROM Reviews'
    #            ' INNER JOIN Users ON Reviews.userID=Users.userID'
    #            ' WHERE productID="{0}"'.format(ID))
    cur.execute('SELECT * FROM Reviews WHERE productID="{0}"'.format(ID))
    reviewList = cur.fetchall()
    message = ""
    error = None
    if request.method == 'POST':
        if request.form["submitButton"] == "Logga ut":
            session.pop('loggedin', None)
            session.pop('id', None)
            session.pop('name', None)
            cur.close()
            return render_template("product.html", prodData=prodData, reviewList=reviewList, )

        if request.form['submitButton'] == "Lägg i kundvagn":
            if not 'loggedin' in session:
                error = 'Du måste logga in för att lägga till varor i din kundvagn'
                cur.close()
                return render_template("product.html", prodData=prodData, error=error, reviewList=reviewList)
            cur.execute('SELECT stock FROM Products WHERE productID={0}'.format(ID))
            stock = cur.fetchall()
            if stock[0][0] == 0:
                message = "Tyvärr är denna vara slut i lager just nu"
                return render_template("product.html", prodData=prodData, message=message, reviewList=reviewList,
                                       user=session['name'])
            result = cur.execute('SELECT * FROM Cart WHERE userID={0} AND productID={1}'.format(session['id'], ID))
            if result > 0:
                result = cur.fetchall()
                amount = int(result[0][2]) + 1
                if amount > stock[0][0]:
                    cur.close()
                    message = "Du har redan {0} st av denna produkt, tyvärr har vi inte fler i vårat lager".format(
                        amount - 1)
                    return render_template("product.html", prodData=prodData, message=message, reviewList=reviewList,
                                           user=session['name'])
                else:
                    cur.execute('UPDATE Cart SET amount={0} WHERE userID={1} AND productID={2}'.format(str(amount),
                                                                                                       session['id'],
                                                                                                       ID))
                    mysql.connection.commit()
                    cur.close()
                    message = "Tack för din beställning! Du har nu {0} st i din varukorg!".format(amount)
                    return render_template("product.html", prodData=prodData, message=message, reviewList=reviewList,
                                           user=session['name'])
            else:
                cur.execute('INSERT INTO Cart(userID, productID, amount) VALUES(%s, %s, %s)',
                            (str(session['id']), str(ID), 1))
                mysql.connection.commit()
                cur.close()
                message = "Tack för din beställning!"
                return render_template("product.html", prodData=prodData, message=message, reviewList=reviewList,
                                       user=session['name'])

        if request.form['submitButton'] == 'Recensera produkt':
            if not 'loggedin' in session:
                error = 'Du måste logga in för att recensera produkter'
                cur.close()
                return render_template("product.html", prodData=prodData, error=error, reviewList=reviewList)
            result = cur.execute('SELECT * FROM Orders WHERE userID={0} AND productID={1}'.format(session["id"], ID))
            if result > 0:
                result = cur.execute(
                    'SELECT * FROM Reviews WHERE userID={0} AND productID={1}'.format(session["id"], ID))
                if result > 0:
                    error = "Du har redan recenserat denna produkt!"
                    cur.close()
                    return render_template("product.html", prodData=prodData, error=error, reviewList=reviewList,
                                           user=session['name'])
                else:
                    grade = request.form["grade"]
                    desc = request.form["desc"]
                    cur.execute('INSERT INTO Reviews(userID, productID, grade, content, name)'
                                ' VALUES("{0}", "{1}", "{2}", "{3}", "{4}")'.format(session["id"], ID, grade, desc, session['name']))
                    mysql.connection.commit()
                    error = "Tack för din recension! :)"
                    cur.close()
                    return render_template("product.html", prodData=prodData, error=error, reviewList=reviewList,
                                           user=session['name'])
            else:
                error = 'Du måste ha köpt produkten för att recensera den'
                cur.close()
                return render_template("product.html", prodData=prodData, error=error, reviewList=reviewList,
                                       user=session['name'])
    cur.close()
    if 'loggedin' in session:
        return render_template("product.html", prodData=prodData, message=message, reviewList=reviewList,
                               user=session['name'])
    return render_template("product.html", prodData=prodData, message=message, reviewList=reviewList)


@app.route('/cart', methods=['GET', 'POST'])
def getCart():
    if not 'loggedin' in session:
        return redirect(url_for('start_page'))
    cur = mysql.connection.cursor()
    resultValue = cur.execute("SELECT * FROM Cart WHERE userID={0}".format(str(session['id'])))
    # If the user has a shopping cart
    if resultValue > 0:
        try:
            cartDetails = cur.fetchall()
            resultValue = cur.execute("SELECT * FROM Products WHERE productID in (SELECT productID FROM Cart WHERE userID={0})".format(str(session['id'])))
            productDetails = cur.fetchall()
            i = 0
            total = 0
            ID = cur.execute("SELECT MAX( orderID ) FROM Orders")
            IDValue = cur.fetchone()[0]
            if IDValue is None:
                IDValue = 1
            for products in productDetails:
                if request.method == 'POST':
                    if request.form['submitButton'] == 'Lägg order':
                        cur.execute('SELECT stock, productName FROM Products WHERE productID={0}'.format(products[0]))
                        amount = cur.fetchone()
                        if amount[0] < cartDetails[i][2]:
                            message = "Det finns endast " + str(amount[0]) + " antal kvar av produkten " + str(amount[1])
                            mysql.connection.rollback()
                            cur.close()
                            return render_template('cart.html', productDetails=productDetails, cartDetails=cartDetails, message=message, len=len(productDetails), total="Total: " + str(total))
                        cur.execute('UPDATE Products SET stock=stock-{0} WHERE productID={1}'.format(cartDetails[i][2], products[0]))
                        cur.execute('INSERT INTO Orders(orderID, userID, productID, Amount, price) VALUES(%s, %s, %s, %s, %s)',(IDValue + 1, str(session['id']), str(products[0]), cartDetails[i][2], products[2]))
                total += products[2] * cartDetails[i][2]
                i += 1
            if request.method == 'POST':
                if request.form['submitButton'] == 'Lägg order':
                    cur.execute('DELETE FROM Cart WHERE userID="{0}"'.format(str(session['id'])))
                    mysql.connection.commit()
                    cur.close()
                    return render_template('cart.html', message="Order skickad", len=0)
                elif request.form['submitButton'] == 'Töm varukorg':
                    cur.execute('DELETE FROM Cart WHERE userID="{0}"'.format(str(session['id'])))
                    mysql.connection.commit()
                    cur.close()
                    return render_template('cart.html', message="Din varukorg är nu tom", len=0)
                elif request.form['submitButton'] == 'Ta bort en':
                    prodID = request.form["productValue"]
                    cur.execute('SELECT amount FROM Cart WHERE userID="{0}" AND productID="{1}"'.format(str(session['id']), prodID))
                    result = cur.fetchall()
                    amount = int(result[0][0]) - 1
                    # If 0 of product left, delete product from order
                    if amount == 0:
                        cur.execute('DELETE FROM Cart WHERE userID="{0}" AND productID="{1}"'.format(str(session['id']), prodID))
                    # Else decrease amount by 1
                    else:
                        cur.execute('UPDATE Cart SET amount={0} WHERE userID="{1}" AND productID={2}'.format(amount, str(session['id']), prodID))
                    mysql.connection.commit()
                    resultValue = cur.execute("SELECT * FROM Cart WHERE userID=" + str(session['id']))
                    # If the user still has a shopping cart
                    if resultValue > 0:
                        # Update values of cart and products
                        cartDetails = cur.fetchall()
                        resultValue = cur.execute("SELECT * FROM Products WHERE productID in (SELECT productID FROM Cart WHERE userID=" + str(session['id']) + ")")
                        productDetails = cur.fetchall()
                        total = 0
                        i=0
                        for products in productDetails:
                            total += products[2] * cartDetails[i][2]
                            i += 1
                        cur.close()
                        return render_template('cart.html', productDetails=productDetails, cartDetails=cartDetails,
                                            message="Kundvagn", len=len(productDetails), total="Total: " + str(total))
                    else:
                        cur.close()
                        return render_template('cart.html', message="Din kundvagn är tom", len=0)                  
                elif request.form['submitButton'] == 'Ta bort alla':
                    prodID = request.form["productValue"]
                    cur.execute('DELETE FROM Cart WHERE userID="{0}" AND productID="{1}"'.format(str(session['id']), prodID))
                    mysql.connection.commit()
                    resultValue = cur.execute("SELECT * FROM Cart WHERE userID=" + str(session['id']))
                    # If the user still has a shopping cart
                    if resultValue > 0:
                        # Update values of cart and products
                        cartDetails = cur.fetchall()
                        resultValue = cur.execute("SELECT * FROM Products WHERE productID in (SELECT productID FROM Cart WHERE userID=" + str(session['id']) + ")")
                        productDetails = cur.fetchall()
                        total = 0
                        i=0
                        for products in productDetails:
                            total += products[2] * cartDetails[i][2]
                            i += 1
                        cur.close()
                        return render_template('cart.html', productDetails=productDetails, cartDetails=cartDetails,
                                            message="Kundvagn", len=len(productDetails), total="Total: " + str(total))
                    else:
                        cur.close()
                        return render_template('cart.html', message="Din kundvagn är tom", len=0)
            else:
                return render_template('cart.html', productDetails=productDetails, cartDetails=cartDetails, message="Kundvagn", len=len(productDetails), total="Total: " + str(total))
        except:
            message = "Servern är upptagen, försök igen senare"
            mysql.connection.rollback()
            cur.close()
            return render_template('cart.html', productDetails=productDetails, cartDetails=cartDetails, message=message, len=len(productDetails), total="Total: " + str(total))
    else:
        return render_template('cart.html', message="Din kundvagn är tom", len=0)


@app.route('/upload', methods=['GET','POST'])
def getFile():
    cur = mysql.connection.cursor()
    resultValue = cur.execute('SELECT * FROM Users WHERE userID="{0}" and Role="Admin"'.format(session["id"]))
    if not resultValue > 0 or not 'loggedin' in session:
        cur.close()
        return redirect(url_for('start_page'))
    cur.execute('SELECT productID, productName FROM Products WHERE active=1')
    activeProductList = cur.fetchall()
    cur.execute('SELECT productID, productName FROM Products WHERE active=0')
    unactiveProductList = cur.fetchall()
    if request.method == 'POST':
        if request.form['submitButton'] == 'Registrera produkt':
            f = request.files['file']
            message = secure_filename(f.filename)
            path = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
            f.save(path)
            prodDetails = request.form
            name = prodDetails["name"]
            price = prodDetails["price"]
            stock = prodDetails["stock"]
            desc = prodDetails["desc"]
            filename = secure_filename(f.filename)
            cur.execute("INSERT INTO Products(productID, productName, price, stock, description, filepath, active) VALUES(%s, %s, %s, %s, %s, %s, %s)",
                            (None, name, price, stock, desc, filename, 1))
            mysql.connection.commit()
            cur.close()
            message = "Produkt sparad med bild {0}".format(desc)
            return render_template("addProduct.html", message=message, activeProductList=activeProductList, unactiveProductList=unactiveProductList)
        if request.form['submitButton'] == 'Ta bort produkt':
            product = request.form['activeProduct']
            cur.execute("UPDATE Products SET active=0 WHERE productID={0}".format(product))
            mysql.connection.commit()
            cur.close()
            message = "{0} är nu en inaktiv produkt".format(product)
            return render_template("addProduct.html", message=message, activeProductList=activeProductList, unactiveProductList=unactiveProductList)
        if request.form['submitButton'] == 'Lägg till produkt igen':
            product = request.form['unactiveProduct']
            cur.execute("UPDATE Products SET active=1 WHERE productID={0}".format(product))
            mysql.connection.commit()
            cur.close()
            message = "{0} är nu en aktiv produkt igen!".format(product)
            return render_template("addProduct.html", message=message, activeProductList=activeProductList, unactiveProductList=unactiveProductList)
        if request.form['submitButton'] == 'Addera till lager':
            product = request.form['products']
            amount = request.form['amount']
            cur.execute("UPDATE Products SET stock=stock+{0} WHERE productID={1}".format(amount,product))
            mysql.connection.commit()
            cur.close()
            message = "Adderat {0} st {1} till lagret".format(amount,product)
            return render_template("addProduct.html", message=message, activeProductList=activeProductList, unactiveProductList=unactiveProductList)
        if request.form['submitButton'] == 'Uppdatera produkt':
            product = request.form['products']
            name = request.form['name']
            price = request.form['price']
            desc = request.form['desc']
            f = request.files['file']
            filename = secure_filename(f.filename)
            if request.files['file'].filename != '':
                path = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
                f.save(path)
            cur.execute("UPDATE Products SET"
                        " productName= CASE WHEN '{0}' != '' THEN '{0}' ELSE productName END,"
                        " price= CASE WHEN '{1}' != '' THEN '{1}' ELSE price END,"
                        " description= CASE WHEN '{2}' != '' THEN '{2}' ELSE description END,"
                        " filepath= CASE WHEN '{3}' != '' THEN '{3}' ELSE filepath END"
                        " WHERE productID='{4}'".format(name, price, desc, filename ,product))
            mysql.connection.commit()
            cur.close()
            message = "Uppdaterat produkt"
            return render_template("addProduct.html", message=message, activeProductList=activeProductList, unactiveProductList=unactiveProductList)
    message=""
    return render_template('addProduct.html', activeProductList=activeProductList, unactiveProductList=unactiveProductList)



@app.route("/login", methods=["GET", "POST"])
def login_register():
    if 'loggedin' in session:
            return redirect(url_for('start_page'))
    if request.method == "POST" and 'name' in request.form:
        userDetails = request.form
        email = userDetails["email"]
        password = userDetails["password"]
        name = userDetails["name"]
        phoneNumber = userDetails["number"]
        Customer = "Customer"
        cur = mysql.connection.cursor()
        try:
            cur.execute("INSERT INTO Users(userID, email, password, name, phoneNumber, role) VALUES(%s, %s, %s, %s, %s, %s)", (None, email, password, name, phoneNumber, Customer))
            mysql.connection.commit()
            message = "Användare tillagd"
        except:
            mysql.connection.rollback()
            message = "Användaren finns redan"
        cur.close()
        return render_template("login.html", message=message)
    if request.method == "POST" and not 'name' in request.form:
        userDetails = request.form
        email = userDetails["email"]
        password = userDetails["password"]
        cur = mysql.connection.cursor()
        cur.execute("SELECT * FROM Users WHERE email = %s AND password = %s", (email, password))
        user = cur.fetchone()
        if user:
            session['loggedin'] = True
            session['id'] = user[0]
            session['name'] = user[3]
            cur.close()
            return redirect(url_for('start_page'))
            #message = "Succesfully logged in! Welcome " + session['name']
        else:
            message = "Fel lösenord eller användarnamn!"
        cur.close()
        return render_template("login.html", message=message)
    return render_template("login.html", title="Login Page")

@app.route("/", methods=["GET", "POST"])
def start_page():
    if request.method == "POST":
        session.pop('loggedin', None)
        session.pop('id', None)
        session.pop('name', None)
    cur = mysql.connection.cursor()
    resultValue = cur.execute("SELECT * FROM Products WHERE active=1")
    if resultValue > 0:
        productDetails = cur.fetchall()
        if 'loggedin' in session:
            cur.close()
            return render_template('start.html', productDetails=productDetails, user=str(session["name"]))
        cur.close()
        return render_template('start.html', productDetails=productDetails)
    cur.close()
    return render_template('start.html', user=session["name"])


if __name__ == "__main__":
    app.run()

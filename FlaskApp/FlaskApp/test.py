try:
    from __init__ import app
    import unittest

except Exception as e:
    print("Some modules are missing {} ").format(e)


class FlaskTest(unittest.TestCase):
    # check for response 200
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get("/login")
        statuscode = response.status_code
        self.assertEquals(statuscode, 200)

if __name__ == "__main__":
    unittest.main()